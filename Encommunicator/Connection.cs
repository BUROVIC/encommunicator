﻿using Encommunicator.Encryption;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Threading;

namespace Encommunicator
{
    enum ConnectionType
    {
        WithoutEncryption,
        EncryptionInitiator,
        EncryptionReceiver
    }
    class Connection
    {
        Socket companionSocket;
        NetworkStream companionStream;

        public delegate void MessageHandler(Message message);
        public event MessageHandler MessageArrived;

        public delegate void DisconnectionHandler();
        public event DisconnectionHandler Disconnected;

        BinaryFormatter formatter = new BinaryFormatter();

        ConnectionType Type { get; set; }

        RSACryptoServiceProvider RSA;
        byte[] AESKey;

        public Connection(Socket socket, ConnectionType connectionType)
        {
            Type = connectionType;

            companionSocket = socket;

            companionStream = new NetworkStream(companionSocket);

            Thread thread = new Thread(HandleMessage);
            thread.Start(companionSocket);

            if (Type == ConnectionType.EncryptionInitiator)
            {
                // Send RSA public key
                RSA = new RSACryptoServiceProvider();
                SendInternal(new Message(MessageType.RSAKey, RSA.ExportCspBlob(false)));
            }
        }

        static object sendLocker = new object();

        public void Send(Message message)
        {
            lock (sendLocker)
            {
                if (Type == ConnectionType.WithoutEncryption)
                {
                    SendInternal(message);
                }
                else
                {
                    if (AESKey != null)
                    {
                        SendInternal(new Message(message.Type, AESEncrypter.Encrypt(message.Bytes, AESKey)));
                    }
                }
            }
        }

        void SendInternal(Message message)
        {
            formatter.Serialize(companionStream, message);
        }

        void HandleMessage(object o)
        {
            while (true)
            {
                try
                {
                    var message = (Message)formatter.Deserialize(companionStream);

                    if (Type == ConnectionType.WithoutEncryption)
                    {
                        MessageArrived?.Invoke(message);
                    }
                    else if (Type == ConnectionType.EncryptionReceiver && message.Type == MessageType.RSAKey)
                    {
                        // Receive RSA open key
                        RSA = new RSACryptoServiceProvider();
                        RSA.ImportCspBlob(message.Bytes);

                        // Send encrypted AES key
                        AESKey = Aes.Create().Key;
                        SendInternal(new Message(MessageType.AESKey, RSA.Encrypt(AESKey, false)));
                    }
                    else if (Type == ConnectionType.EncryptionInitiator && message.Type == MessageType.AESKey)
                    {
                        // Receive encrypted AES key
                        AESKey = RSA.Decrypt(message.Bytes, false);
                    }
                    else
                    {
                        if (AESKey != null)
                        {
                            MessageArrived?.Invoke(new Message(message.Type, AESEncrypter.Decrypt(message.Bytes, AESKey)));
                        }
                    }
                }
                catch
                {
                    companionSocket.Shutdown(SocketShutdown.Both);
                    companionSocket.Disconnect(true);
                    Disconnected?.Invoke();
                    return;
                }
            }
        }
    }
}
