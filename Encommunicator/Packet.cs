﻿using System;
using System.IO;
using System.Text;
using System.Windows.Media.Imaging;

namespace Encommunicator
{
    public enum MessageType
    {
        RSAKey,
        AESKey,
        Text,
        Video,
        Audio
    }

    [Serializable]
    class Message
    {
        public MessageType Type { get; private set; }
        public byte[] Bytes { get; private set; }

        public Message(MessageType type, byte[] bytes)
        {
            Type = type;
            Bytes = bytes;
        }

        public string Text { get { return Encoding.UTF8.GetString(Bytes); } }

        public BitmapImage BitmapImage
        {
            get
            {
                BitmapImage bitmapImg = new BitmapImage();
                bitmapImg.BeginInit();
                bitmapImg.StreamSource = new MemoryStream(Bytes);
                bitmapImg.EndInit();

                return bitmapImg;
            }
        }
    }
}
