﻿using NAudio.Wave;
using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Linq;
using System.Windows.Media;
using System.Drawing.Imaging;

namespace Encommunicator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        Connection companionConnection { get; set; }

        const string ONLINE_INDICATOR_COLOR = "#00FF00";
        const string OFFLINE_INDICATOR_COLOR = "#FF0000";

        bool isEncrypted = true;

        VideoCaptureDevice videoCaptureDevice =
                    new VideoCaptureDevice(new FilterInfoCollection(FilterCategory.VideoInputDevice)[0].MonikerString);

        WaveFormat waveFormat = new WaveFormat(44100, 1);
        WaveIn waveIn = new WaveIn();
        WaveOut waveOut = new WaveOut();
        BufferedWaveProvider bufferedWaveProvider;

        static Random random = new Random();

        bool _isConnected;
        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
            set
            {
                _isConnected = value;
                ConnectionIndicationColor = value ? ONLINE_INDICATOR_COLOR : OFFLINE_INDICATOR_COLOR;
            }
        }

        string _connectionIndicationColor;
        public string ConnectionIndicationColor
        {
            get { return _connectionIndicationColor; }
            set
            {
                _connectionIndicationColor = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            Closing += OnWindowClosing;

            DataContext = this;
            InitializeComponent();

            IsConnected = false;

            // Start listening to incoming connections.
            try
            {
                var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

                socket.Bind(new IPEndPoint(IPAddress.Any, 2048));
                socket.Listen(0);

                socket.BeginAccept((IAsyncResult ar) =>
                {
                    var companionSocket = socket.EndAccept(ar);
                    switch (MessageBox.Show(
                        $"Start communication with {(companionSocket.RemoteEndPoint as IPEndPoint).Address}?",
                        "Incoming connection...",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Question))
                    {
                        case MessageBoxResult.Yes:
                            Connect(companionSocket, false);
                            break;
                        default:
                            break;
                    }

                }, null);
            }
            catch (Exception exception)
            {
                DisplayErrorException(exception);
                Application.Current.Shutdown();
            }
        }

        private void VideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            // Send image with reduced quality.

            long quality = 10L;
            using (var ms = new MemoryStream())
            {
                EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                ImageCodecInfo imageCodec = ImageCodecInfo.GetImageEncoders().FirstOrDefault(o => o.FormatID == ImageFormat.Jpeg.Guid);
                EncoderParameters parameters = new EncoderParameters(1);
                parameters.Param[0] = qualityParam;
                eventArgs.Frame.Save(ms, imageCodec, parameters);
                companionConnection?.Send(new Message(MessageType.Video, ms.ToArray()));
            }

        }

        void waveSource_DataAvailable(object sender, WaveInEventArgs e)
        {
            companionConnection?.Send(new Message(MessageType.Audio, e.Buffer.ToArray()));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        void Connect(Socket companionSocket, bool connectionInitiated)
        {
            var connectionType = isEncrypted ?
                (connectionInitiated ? ConnectionType.EncryptionInitiator : ConnectionType.EncryptionReceiver)
                : ConnectionType.WithoutEncryption;

            companionConnection = new Connection(companionSocket, connectionType);

            companionConnection.MessageArrived += OnMessage;
            companionConnection.Disconnected += OnDisconnect;

            IsConnected = true;

            // Start capturing video.
            try
            {
                videoCaptureDevice.NewFrame += VideoCaptureDevice_NewFrame;
                videoCaptureDevice.Start();
            }
            catch (Exception exception)
            {
                DisplayErrorException(exception);
            }

            // Start capturing audio.
            try
            {
                waveIn.WaveFormat = waveFormat;
                waveIn.DataAvailable += new EventHandler<WaveInEventArgs>(waveSource_DataAvailable);

                waveIn.StartRecording();

                bufferedWaveProvider = new BufferedWaveProvider(waveFormat) { DiscardOnBufferOverflow = true };

                waveOut.Init(bufferedWaveProvider);
                waveOut.Play();
            }
            catch (Exception exception)
            {
                DisplayErrorException(exception);
            }
        }

        void DisplayErrorException(Exception exception)
        {
            MessageBox.Show(exception.Message, exception.Source + " error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        #region Connection events
        private void OnMessage(Message message)
        {
            switch (message.Type)
            {
                case MessageType.Text:
                    Dispatcher.Invoke(() =>
                    {
                        TextBlockChat.Text += "Companion: " + message.Text + "\n";
                    });
                    break;
                case MessageType.Video:
                    Dispatcher.Invoke(() =>
                    {
                        CameraFrame.Source = message.BitmapImage as ImageSource;
                    });
                    break;
                case MessageType.Audio:
                    try
                    {
                        bufferedWaveProvider.AddSamples(message.Bytes, 0, message.Bytes.Length);
                    }
                    catch (Exception) { }
                    break;
                default:
                    break;
            }

        }
        private void OnDisconnect()
        {
            IsConnected = false;
        }
        #endregion

        #region UI events
        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var companionSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                companionSocket.Connect(TextBoxIp.Text, 2048);
                Connect(companionSocket, true);
            }
            catch (Exception exception)
            {
                DisplayErrorException(exception);
            }
        }

        private void ButtonSendChatMessage_Click(object sender, RoutedEventArgs e)
        {
            TextBlockChat.Text += "Me: " + TextBoxChatMessage.Text + "\n";

            companionConnection.Send(new Message(MessageType.Text, Encoding.UTF8.GetBytes(TextBoxChatMessage.Text)));

            TextBoxChatMessage.Text = "";
        }
        #endregion

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);

            waveIn.StopRecording();
            waveIn.Dispose();

            waveOut.Stop();
            waveOut.Dispose();

            videoCaptureDevice.Stop();
        }
    }
}
